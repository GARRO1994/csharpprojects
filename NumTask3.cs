﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Num
    {
       int up, dow;
       public double Real { get { return (double)up / (double)dow; } }

        public Num(int _up, int _dow)
        {
            up = _up;
            dow = _dow;
        }

        public Num() { }

        public static Num operator+(Num a, Num b)
        {
           Num num = new Num();
            num.up = a.up * b.dow + b.up * a.dow;
            num.dow = a.dow * b.dow;
           return num;
        }
        public static Num operator-(Num a, Num b)
        {
            Num num = new Num();
            num.up = a.up * b.dow - b.up * a.dow;
            num.dow = a.dow * b.dow;
            return num;
        }
        public static Num operator*(Num a, Num b)
        {
            Num num = new Num();
            num.up = a.up * b.up;
            num.dow = a.dow * b.dow;
            return num;
        }
        public static Num operator/(Num a, Num b)
        {
            Num num = new Num();
            num.up = a.up * b.dow;
            num.dow = a.dow * b.up;
            return num;
        }
        

        public override string ToString()
        {
            return up + "/" + dow;
        }
    }
}
