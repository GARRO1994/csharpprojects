﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {
        public static string Word(string a, string b, string c)
        {
            return a + " " + b + " " + c;
        }

        static void Main(string[] args)
        {

            string name;
            string surname;
            string city;

            Console.Write("Введите имя: ");
            name = Console.ReadLine();

            Console.Write("Введите фамилию: ");
            surname = Console.ReadLine();

            Console.Write("Введите город: ");
            city = Console.ReadLine();

            //Вывод в центре
            // Console.SetCursorPosition(Console.WindowWidth / 2, Console.WindowHeight / 2);
            //Console.WriteLine(name + " " + surname + " " + city);

            Console.Write(Word(name, surname, city));//через метод

            Console.ReadLine();
        }
    }
}
