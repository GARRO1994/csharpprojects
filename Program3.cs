﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            int a = 5; 
            int b = 3;
            a = a + b;
            b = a - b;
            a = a - b;
            //int t = a;
            // a = b;    это с использованием 3 переменных
            // b = t;

            Console.Write("a " + a + " b " + b);
            Console.ReadLine();
        }
    }
}
