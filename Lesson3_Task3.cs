﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Num one = new Num(3, 8);
            Num two = new Num(1, 2);
            
            Console.WriteLine(one + two);
            Console.WriteLine(one - two);
            Console.WriteLine(one * two);
            Console.WriteLine(one / two);
            Console.WriteLine(one.Real);
            Console.WriteLine(two.Real);
            Console.ReadLine();
        }
    }
}
