﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {
      public static int Min(int a, int b, int c)
        {
            int d = Math.Min(a, b);
            int e = Math.Min(d, c);
            return e;
        }
        static void Main(string[] args)
        {
            int q = Convert.ToInt16(Console.ReadLine());
            int w = Convert.ToInt16(Console.ReadLine());
            int r = Convert.ToInt16(Console.ReadLine());
            
            Console.Write(Min(q, w, r));
            Console.ReadLine();
            

          
        }
    }
}
