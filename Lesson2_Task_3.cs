﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int a = 0;
            int sum = 0;
            do
            {
                a = Convert.ToInt32(Console.ReadLine());
                if (a % 2 != 0)
                {
                    sum += a;
                }
            }
            while (a != 0);

            Console.Write("STOP ");
            Console.Write(sum);
            Console.ReadLine();
            

          
        }
    }
}
