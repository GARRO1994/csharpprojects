﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            string surname;
            double age;
            double height;
            double weight;

            Console.Write("Введите имя: ");
            name = Console.ReadLine();

            Console.Write("Введите фамилию: ");
            surname = Console.ReadLine();

            Console.Write("Введите возраст: ");
             age = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите рост: ");
            height = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите вес: ");
            weight = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(name + " " + surname + " " + age + " " + height + " " + weight);
            Console.ReadLine();
        }
    }
}
