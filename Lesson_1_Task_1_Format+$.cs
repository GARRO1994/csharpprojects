﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            string surname;
            double age;
            double height;
            double weight;
            
        
            Console.Write("Введите имя: ");
             name = Console.ReadLine();

            Console.Write("Введите фамилию: ");
             surname = Console.ReadLine();

            Console.Write("Введите возраст: ");
             age = double.Parse(Console.ReadLine());
            
            Console.Write("Введите рост: ");
             height = double.Parse(Console.ReadLine());
        
            Console.Write("Введите вес: ");
             weight = double.Parse(Console.ReadLine());

            Console.WriteLine($"Name: {name}  Surname: {surname}");
            Console.WriteLine("{0:0.##}", age);
            Console.WriteLine("{0:0.##}", height);
            Console.WriteLine("{0:0.##}", weight);
            Console.ReadLine();

        }
    }
}
