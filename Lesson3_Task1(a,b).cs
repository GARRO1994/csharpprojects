﻿
using System;
using System.Threading;
namespace ConsoleApp4
{
    struct Complex
    {
        public double a;
        public double b;

        public Complex Plus(Complex x)
        {
            Complex y;
            y.a = a + x.a;
            y.b = b + x.b;
            return y;
        }

        public Complex Multi(Complex x)
        {
            Complex y;
            y.a = b * x.a + a * x.b;
            y.b = b * x.b - a * x.a;
            return y;
        }

        public Complex Minus(Complex x)
        {
            Complex y;
            y.a = a - x.a;
            y.b = b - x.b;
            return y;
        }

        public string ToString()
        {
            return a + "+" + b;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Complex complex1;
            complex1.b = 7;
            complex1.a = 3;
            Complex complex2;
            complex2.b = 6;
            complex2.a = 11;
            Complex result = complex1.Plus(complex2);//(a+a) + (b+b)
            Console.WriteLine(result.ToString());
            result = complex1.Multi(complex2);      //((b*a)+(a*b)) + ((b*b)+(a*a))
            Console.WriteLine(result.ToString());
            result = complex1.Minus(complex2);      //(a-a) + (b-b)
            Console.WriteLine(result.ToString());
            Console.ReadLine();
        }
    }
}

