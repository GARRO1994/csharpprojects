﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {
        static double Result(double x1, double x2, double y1, double y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
        static void Main(string[] args)
        {

            double x1 = 5;
            double x2 = 3;
            double y1 = 4;
            double y2 = 2;
            // это рассчет по формуле
            //double r = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2))
            // Console.Write("{0:0.##}", r);
            Console.Write("{0:0.##}", Result(x1, x2, y1, y2)); // а здесь по методу
            Console.ReadLine();
        }
    }
}
