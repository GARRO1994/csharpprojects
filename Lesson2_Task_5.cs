﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Введите вес");
            double weight = Convert.ToInt16(Console.ReadLine());

            Console.WriteLine("Введите рост");
            int height = Convert.ToInt16(Console.ReadLine());

            double index = weight / (height * height);
            Console.WriteLine("Индекс вашего тела равен- " + "{0:0.####}", index);

            if (index > 0.002 && index < 0.003)
            {
                Console.WriteLine("Вы в норме");
            }

            double a;
            double b;
            if (index > 0.003)
            {
                 a = index - 0.0029;
                b = a * (height * height);
                Console.WriteLine("Вам бы сбросить " + b + " кг");
            }

            if (index < 0.002)
            {
                a = 0.002 - index;
                b = a * (height * height);
                Console.WriteLine("Кушайте больше каши, наберите " + b + " кг");
            }
               

            Console.ReadLine();
            

          
        }
    }
}
